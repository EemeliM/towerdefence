City Defence

Created by  Eemeli Manninen
			0418640
			eemeli.manninen@student.lut.fi

Project started on 07/10/2017

///////////////////////////////////////////

TODO, ideas & known bugs to probably to be fixed:

BUGS 

TODO 
	1 title screen (dialog box over main game area)
	original title art which is inline with the story of the game
	link to the decription video in youtube (new tab)
		1 button to start game
		2 button to close program (maybe unescessary if deployed as webapp)
		3 this pops up when game is over (player dead && enemies all dead)
		4 game can be ended whenever to return to this screen

IDEAS 
	1 Turret type that does more than 1 damage at a time (super boss killer)

	2 more levels
		1 level x/you
		2 game difficulty should be about replacementing turrets optimally
		-> there should be selling penalty, but a very small one
		-> micromanaging turrets is vital
		3 make turrets situational
			1 basic super cheap
			2 longrange/buy and forget
			3 fast fire rate / short range <- the essence of the game