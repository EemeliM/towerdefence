﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameWorldHandler : MonoBehaviour {
	
    // Game-spesific variables
	public static int Health;
	public static int Money;
	public static int Score;

    // General UI
	public Text M;
	public Text H;
	public Text S;
	public Text GOText;
    public Text Wave;
    public static bool gameOver;
    public static bool isShopOpen = false;
    public static bool isTurretInfoOpen = false;
    public Button startNewGameButton;
    public Image MainmenuImage;
    public Text Credits;

    // Enemy handling
    private List<string> coordinates;
    public static List<EnemyClass> enemyList;
	public static List<TowerClass> towerList;
    public static List<GameObject> allEnemiesCurrentlyOnScreen;
    public static List<List<float>> wavesOfGame;
    private int waveNumber;
    public GameObject newWaveButton;
    private bool spawningInProgress;
    private List<int> thisWaveEnemies;
    private float thisWaveWaitTime;
    private float thisWaveSpawnTime;
    public static int towersOnScreen;

    // Tower - Tile relationships, which turret is on which tile
    public static Dictionary<GameObject,GameObject> turretTiles = new Dictionary<GameObject,GameObject>();

    // All tiles so we can show them all after game reset
    public static List<GameObject> allTiles = new List<GameObject>();
    public static bool isTileListOriginal = true;

	// Initializes UI elements and enemies available for the game
	void Start() {
		
        // Basic game information
		Health = 100;
		Money = 3600;
        Score = 0;

		gameOver = false;
        spawningInProgress = false;
        waveNumber = 0;
        towersOnScreen = 0;
        thisWaveSpawnTime = Time.time;
        startNewGameButton.onClick.AddListener(StartNewGame);
        newWaveButton.SetActive(true);
        isShopOpen = false;
        isTurretInfoOpen = false;
        allEnemiesCurrentlyOnScreen = new List<GameObject>();
        wavesOfGame = new List<List<float>>();
        towerList = new List<TowerClass>();
        thisWaveEnemies = new List<int>();
        enemyList = new List<EnemyClass>();

        // Turret info, accessed from TowerBehaviour
        if (GameObject.Find("TurretInfo"))
            GameObject.Find("TurretInfo").SetActive(false);

        // Shop, accessed from TileBehaviour
        if (GameObject.Find("Shop"))
            GameObject.Find("Shop").SetActive(false);

        // Map layout: Path consists of pathnodes (circles) and direction changes.
		List<string> PathNodes = new List<string> ();
		List<string> Directions = new List<string> ();
        coordinates = new List<string>();
		PathNodes.AddRange(("Circle0,Circle1,Circle2,Circle3,Circle4,Circle5,Circle6,Circle7,Circle8,Circle9,Circle10").Split(','));
		Directions.AddRange(("d,r,u,r,d,r,d,l,d,r, ").Split(','));
		var dirCount = 0;
		foreach (var node in PathNodes) 
		{
			var obj = GameObject.Find(node).transform.position;
			coordinates.Add(obj.x + " " + obj.y + " " + Directions[dirCount]);
			dirCount++;
		}

        // Save all tiles to list when they are all still listable
        if (isTileListOriginal) 
        {
            foreach (var tile in GameObject.FindGameObjectsWithTag("Tile")) 
            {
                allTiles.Add(tile);
            }
            isTileListOriginal = false; 
        }

		// Enemy class variables
        enemyList = new List<EnemyClass>();
		enemyList = AddEnemyToList (enemyList, 2, 4, "Enemy", 10);
		enemyList = AddEnemyToList (enemyList, 10, 2, "Toughquick", 50);
		enemyList = AddEnemyToList (enemyList, 5, 6, "Superfast", 30);
		enemyList = AddEnemyToList (enemyList, 20, 1, "Supertough", 250);
        enemyList = AddEnemyToList (enemyList, 2000, 1, "Finalboss", 0);

		// Tower class variables
        towerList = new List<TowerClass>();
		towerList = AddTowerToList(towerList, "Basic", 2, 8, "Basic", 1, 600);
        towerList = AddTowerToList(towerList, "Longrange", 2, 20, "Longrange", 1, 1000);
        towerList = AddTowerToList(towerList, "Rapid-fire", 10, 5, "Rapidfire", 1, 5000);

        // Enemy waves
        wavesOfGame = new List<List<float>>();
        wavesOfGame = AddWaveToGame(wavesOfGame, 5, 50, 0, 0, 0, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 5, 50, 10, 0, 0, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 3, 0, 50, 10, 0, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 5, 50, 0, 0, 5, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 5, 75, 0, 25, 0, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 5, 20, 10, 20, 10, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 10, 0, 0, 50, 0, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 5, 0, 25, 25, 10, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 10, 100, 50, 20, 0, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 2, 0, 0, 0, 100, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 0.5f, 0, 0, 0, 0, 1);
        wavesOfGame = AddWaveToGame(wavesOfGame, 4, 0, 0, 0, 100, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 6, 0, 0, 0, 100, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 8, 0, 0, 0, 100, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 10, 0, 0, 0, 100, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 11, 0, 0, 0, 100, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 12, 0, 0, 0, 100, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 13, 0, 0, 0, 100, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 14, 0, 0, 0, 100, 0);
        wavesOfGame = AddWaveToGame(wavesOfGame, 0.05f, 0, 0, 0, 0, 3);

        Wave.text = "Wave: 0/" + wavesOfGame.Count;
	}

	/// <summary>
	/// Adds an enemy to list
	/// </summary>
	/// <returns>List of enemies</returns>
	/// <param name="list">List</param>
	/// <param name="HP">Health of an enemy</param>
	/// <param name="Speed">Speed of an enemy</param>
	/// <param name="ObjectName">Object that is copied to create this enemy</param>
	/// <param name="KillReward">Kill reward</param>
	public List<EnemyClass> AddEnemyToList(List<EnemyClass> list, int HP, float Speed, string ObjectName, int KillReward) 
	{
		EnemyClass addedEnemy = new EnemyClass ();
		addedEnemy.Id = list.Count;
		addedEnemy.HP = HP;
		addedEnemy.Speed = Speed;
		addedEnemy.Object =  GameObject.Find(ObjectName);
		addedEnemy.KillReward = KillReward;
		list.Add (addedEnemy);
		return list;
	}

	/// <summary>
	/// Adds a tower to list
	/// </summary>
	/// <returns>The tower list</returns>
	/// <param name="list">List</param>
	/// <param name="FireSpeed">Fire speed</param>
	/// <param name="Range">Range</param>
	/// <param name="ObjectName">Object that is copied to create this tower</param>
	/// <param name="ProjectileType">Projectile type</param>
	public List<TowerClass> AddTowerToList(List<TowerClass> list, string name, float FireSpeed, float Range, string ObjectName, int ProjectileType, int cost) 
	{
		TowerClass addedTower = new TowerClass();
		addedTower.Id = list.Count;
        addedTower.Name = name;
		addedTower.FireSpeed = FireSpeed;
		addedTower.ProjectileType = ProjectileType;
		addedTower.Range = Range;
		addedTower.cost = cost;
		addedTower.Object = GameObject.Find (ObjectName);
		list.Add (addedTower);
		return list;
	}

    /// <summary>
    /// Adds wave to list of waves
    /// </summary>
    /// <returns>List of waves</returns>
    /// <param name="waves">Waves</param>
    /// <param> name="speed">Speed of enemies</param>
    /// <param name="type1">Nr of type 1 enemies</param>
    /// <param name="type2">Nr of type 2 enemies</param>
    /// <param name="type3">Nr of type 3 enemies.</param>
    /// <param name="type4">Nr of type 4 enemies.</param>
    /// <param name="type5">Nr of type 5 enemies.</param>
    public List<List<float>> AddWaveToGame(List<List<float>> waves,float speed, float type1, float type2, float type3, float type4, float type5) 
    {
        List<float> wave = new List<float>();
        wave.Add(speed);
        wave.Add(type1);
        wave.Add(type2);
        wave.Add(type3);
        wave.Add(type4);
        wave.Add(type5);
        waves.Add(wave);
        return waves;
    }

    // This is called from UI
    /// <summary>
    /// Starts the new wave from level list if possible
    /// </summary>
    public void StartNewWave() 
    {
        // Initialize the wave number, wave speed and wave enemy amount. Rest is done in LateUpdate().
        waveNumber++;
        if (waveNumber <= wavesOfGame.Count)
        {
            Wave.text = "Wave: " + waveNumber + "/" + wavesOfGame.Count;
            spawningInProgress = true;
            newWaveButton.SetActive(false);
            thisWaveSpawnTime = Time.time;
            thisWaveEnemies.Clear();

            var selectedWave = wavesOfGame[waveNumber-1];
            thisWaveWaitTime = 1;
            if (selectedWave[0] != 0)
                thisWaveWaitTime = 1 / selectedWave[0];

            thisWaveEnemies.Add(Mathf.RoundToInt(selectedWave[1]));
            thisWaveEnemies.Add(Mathf.RoundToInt(selectedWave[2]));
            thisWaveEnemies.Add(Mathf.RoundToInt(selectedWave[3]));
            thisWaveEnemies.Add(Mathf.RoundToInt(selectedWave[4]));
            thisWaveEnemies.Add(Mathf.RoundToInt(selectedWave[5]));
        }
    }

    /// <summary>
    /// Starts a new game (hides the main menu)
    /// </summary>
    void StartNewGame()
    {
        startNewGameButton.gameObject.SetActive(false);
        MainmenuImage.gameObject.SetActive(false);
        Credits.gameObject.SetActive(false);
    }

	/// <summary>
    /// Creates a new enemy
    /// </summary>
    /// <param name="enemyType">Given enemy type</param>
    private void CreateNewEnemy(int enemyType) 
	{
        var createdEnemyType = enemyList[enemyType];
        GameObject go = Instantiate(createdEnemyType.Object, new Vector3(0,0,0), new Quaternion(0,0,0,0));
		EnemyBehaviour eb = go.GetComponent(typeof(EnemyBehaviour)) as EnemyBehaviour;
		Rigidbody2D rb = go.GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;
		eb.coordinates = coordinates;
		eb.rb = rb;
        eb.AddEnemyToStart(createdEnemyType);
        allEnemiesCurrentlyOnScreen.Add(go);
	}

    /// <summary>
    /// Deletes all game-spesific game objects from the screen and returns to title screen
    /// </summary>
    public void ExitProgram() 
    {
        //Application.Quit();
        startNewGameButton.gameObject.SetActive(true);
        MainmenuImage.gameObject.SetActive(true);
        Credits.gameObject.SetActive(true);
        startNewGameButton.onClick.RemoveAllListeners();
        GOText.text = "";

        // Remove all enemies and health bars
        foreach (var t in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if (t.name.Contains("(Clone)"))
            {
                Destroy(t);
            }
        }

        // Remove all towers
        foreach (var t in GameObject.FindGameObjectsWithTag("Tower"))
        {
            if (t.name.Contains("(Clone)"))
            {
                Destroy(t);
            }
        }

        // Tiles visible after game reset
        foreach (var tile in allTiles) 
        {
            tile.SetActive(true);
        }

        Start();
    }

    public void EndGame() 
    {
        H.text = "Health: 0";
        GOText.text = "Game over!";
        gameOver = true;

        // Remove health bars that are no longer tracked by towers
        foreach (var t in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if (t.name == "HPFull(Clone)")
            {
                Destroy(t);
            }
        }

    }
        
    /// <summary>
    /// Updates UI, spawns enemies, ends game
    /// </summary>
	public void LateUpdate() 
	{
		if (!gameOver)
		{
			if (Health <= 0) 
			{
                EndGame();
				return;
			}

			M.text = "Money: " + Money;
			H.text = "Health: " + Health;
			S.text = "Score: " + Score;

            // If no enemies are spawning and the created are dead, we can start new wave
            if (!spawningInProgress && allEnemiesCurrentlyOnScreen.Count == 0)
            {
                newWaveButton.SetActive(true);
            }

            // Spawning enemies
            if (spawningInProgress && thisWaveSpawnTime <= Time.time) 
            {
                // Spawn enemies until they run out
                if (thisWaveEnemies[0] > 0) 
                {
                    CreateNewEnemy(0);
                    thisWaveEnemies[0] = thisWaveEnemies[0] - 1;
                    thisWaveSpawnTime = Time.time + thisWaveWaitTime;
                }
                if (thisWaveEnemies[1] > 0) 
                {
                    CreateNewEnemy(1);
                    thisWaveEnemies[1] = thisWaveEnemies[1] - 1;
                    thisWaveSpawnTime = Time.time + thisWaveWaitTime;
                }
                if (thisWaveEnemies[2] > 0) 
                {
                    CreateNewEnemy(2);
                    thisWaveEnemies[2] = thisWaveEnemies[2] - 1;
                    thisWaveSpawnTime = Time.time + thisWaveWaitTime;
                }
                if (thisWaveEnemies[3] > 0) 
                {
                    CreateNewEnemy(3);
                    thisWaveEnemies[3] = thisWaveEnemies[3] - 1;
                    thisWaveSpawnTime = Time.time + thisWaveWaitTime;
                }
                if (thisWaveEnemies[4] > 0) 
                {
                    CreateNewEnemy(4);
                    thisWaveEnemies[4] = thisWaveEnemies[4] - 1;
                    thisWaveSpawnTime = Time.time + thisWaveWaitTime;
                }

                if (thisWaveEnemies[0] == 0 && thisWaveEnemies[1] == 0 && thisWaveEnemies[2] == 0 && thisWaveEnemies[3] == 0 && thisWaveEnemies[4] == 0)
                {
                    // Party is over
                    spawningInProgress = false;
                }          
            }      
		}
	}
}