﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This is the tower class, with different fire speeds and ranges
public class TowerClass 
{
	public int Id;
    public string Name;
	public GameObject Object;
	public float FireSpeed;
	public float Range;
	public int ProjectileType;
	public int cost;
}