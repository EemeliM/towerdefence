﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyClass 
{
	public int Id;
	public int HP;
	public float Speed;
	public GameObject Object;
	public int KillReward;
}