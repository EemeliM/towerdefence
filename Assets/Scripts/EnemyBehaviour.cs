﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EnemyBehaviour : MonoBehaviour {

	private int wayPointCounter;
	public List<string> coordinates;
	public Rigidbody2D rb;
	private EnemyClass enemyInfo;
    public GameObject HPdummy;
    public GameObject HPEmpty;

	/// <summary>
	/// Adds new enemy to the beginning of the path and sets it towards the first waypoint
    /// Called from GameWorldHandlers CreateNewEnemy() method upon creating a new enemy.
	/// </summary>
	/// <param name="enemy">Info of created enemy (HP, speed etc.)</param>
	public void AddEnemyToStart(EnemyClass enemy) 
	{
		enemyInfo = enemy;
		var start = coordinates [0].Split(' ');
		var startx = float.Parse(start [0]);
		var starty = float.Parse(start [1]);
		gameObject.transform.position = new Vector3(startx, starty,0);
		rb.velocity = new Vector2(0,-enemyInfo.Speed);

        // Create a dummy health bar object to follow the enemy if there are no towers on screen to keep track of the enemy health
        HPdummy = Instantiate(GameObject.Find("HPFullDummy"), new Vector3(transform.position.x,transform.position.y + 0.4f ,transform.position.z), new Quaternion(0,0,0,0));
        if (GameWorldHandler.towersOnScreen > 0)
            HPdummy.SetActive(false);

        // Also create the hp bar red background here which is always created by the enemy
        HPEmpty = Instantiate(GameObject.Find("HPEmpty"), new Vector3(transform.position.x,transform.position.y + 0.4f ,transform.position.z), new Quaternion(0,0,0,0));

		wayPointCounter = 1; 
	}

    void OnDestroy() 
    {
        if (HPEmpty != null)
            Destroy(HPEmpty);
        if (HPdummy != null)
            Destroy(HPdummy);

        // Play explosion sounds because why not
        if (GameObject.Find(name.Split('(')[0]) != null)
        {
            GameObject.Find(name.Split('(')[0]).GetComponent<AudioSource>().Play();
        }
    }

	/// <summary>
	/// Tracks all enemies on screen.
	/// If one collides with a waypoint is enemy spesific waycountercount checked and the next in line waypoint selected to the next waypoint.
	/// This also changes the direction of the enemy towards the next waypoint.
	/// If enemy reaches the last waypoint, it is deleted and the players health is depleted.
	/// </summary>
	void LateUpdate() {
		if (coordinates == null)
			return;

        // Update dummy health bar position
        if (GameWorldHandler.towersOnScreen == 0)
        {
            if (HPdummy != null)
            {
                HPdummy.transform.position = new Vector3(transform.position.x, transform.position.y + 0.4f, transform.position.z);
            }
        }
        else
        {
            if (HPdummy != null)
                Destroy(HPdummy);
        }

        // Update hp background position
        if (HPEmpty != null)
        {
            HPEmpty.transform.position = new Vector3(transform.position.x, transform.position.y + 0.4f, transform.position.z);
        }
            
        if (wayPointCounter < coordinates.Count && wayPointCounter > 0) 
		{
			var nextCoordinate = coordinates[wayPointCounter].Split(' ');
			var coordinateValues = new Vector3(float.Parse(nextCoordinate[0]),float.Parse(nextCoordinate[1]), 0);
			var direction = "";
			if (nextCoordinate.Length == 3) direction = nextCoordinate [2];

			var thisCoordinate = gameObject.transform.position;

			// Check if positions are close enough
			// Range change because if speed is too high enemies clip through the coordinate -> Increase the catch window for faster enemies
			var range = 0.05;
			if (enemyInfo.Speed >= 1)
				range = 0.1;
            if (enemyInfo.Speed >= 10)
                range = 0.2;
			if (
				(thisCoordinate.x < coordinateValues.x + range && thisCoordinate.x > coordinateValues.x - range)
				&&
				(thisCoordinate.y < coordinateValues.y + range && thisCoordinate.y > coordinateValues.y - range)) {

				// If close enough we warp to exact position and figure out where to go next
				wayPointCounter++;
				gameObject.transform.position = coordinateValues;
				var newDirection = new Vector3();

                // Enemy movement
				switch (direction) {
				case "u":
					newDirection.x = 0;
					newDirection.y = enemyInfo.Speed;
					break;
				case "r":
					newDirection.x = enemyInfo.Speed;
					newDirection.y = 0;
					break;
				case "d":
					newDirection.x = 0;
					newDirection.y = -enemyInfo.Speed;
					break;
				case "l":
					newDirection.x = -enemyInfo.Speed;
					newDirection.y = 0;
					break;
                    default:
                        newDirection.x = 0;
                        newDirection.y = 0;

                        // If boss enemy makes it to the end, game is instantly over
                        if (enemyInfo.Object.name == "Finalboss")
                        {
                            GameWorldHandler.gameOver = true;
                            GameWorldHandler.allEnemiesCurrentlyOnScreen.Remove(gameObject);
                            Destroy(gameObject);
                            if (GameWorldHandler.towersOnScreen == 0)
                                Destroy(HPdummy);
                            Destroy(HPEmpty);
                            GameObject.Find("GameWorld").GetComponent<GameWorldHandler>().EndGame();
                            break;
                        }

                        GameWorldHandler.allEnemiesCurrentlyOnScreen.Remove(gameObject);
                        Destroy(gameObject);
                        if (GameWorldHandler.towersOnScreen == 0)
                            Destroy(HPdummy);
                        Destroy(HPEmpty);
                        GameWorldHandler.Health = GameWorldHandler.Health - 5;
					break;
				}
				rb.velocity = newDirection;
			}
		}
	}
}