﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TileBehaviour : MonoBehaviour {

    private GameObject tower = null;

    // Shop UI
    public GameObject Shop;
    public Button buyTowerButton1;
    public Button buyTowerButton2;
    public Button buyTowerButton3;
    public Button exitShopButton;

    /// <summary>
    /// Opens shop view if no tower is on this tile
    /// </summary>
    public void OnMouseDown() 
	{
        // Open shop for this tile if nothing else is open
        if (tower == null && !GameWorldHandler.isTurretInfoOpen && !GameWorldHandler.isShopOpen)
        {
            GameWorldHandler.isShopOpen = true;
            buyTowerButton1.onClick.AddListener(() => BuyTower(0));
            buyTowerButton2.onClick.AddListener(() => BuyTower(1));
            buyTowerButton3.onClick.AddListener(() => BuyTower(2));
            exitShopButton.onClick.AddListener(() => ExitShop());
            Shop.SetActive(true);
        }  
	}

    /// <summary>
    /// Creates selected tower using CreateNewTower method, closes shop and unloads shop button listeners.
    /// </summary>
    /// <param name="id">Identifier.</param>
    public void BuyTower(int id)
    {
        var position = gameObject.transform.position;
        var temp = CreateNewTower(id);
        if (temp != null)
        {
            temp.transform.position = position;
            Shop.SetActive(false);
            GameWorldHandler.isShopOpen = false;
            unloadTileScripts();
        }
    }

    /// <summary>
    /// Ceates a new tower to this tiles location.
    /// </summary>
    /// <returns>Instance of new tower</returns>
    /// <param name="towerType">Tower type</param>
    public GameObject CreateNewTower(int towerType)
    {
        var addedTower = GameWorldHandler.towerList[towerType];
        if (addedTower.cost <= GameWorldHandler.Money)
        {
            GameObject go = Instantiate(addedTower.Object, new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0));
            TowerBehaviour tb = go.GetComponent(typeof(TowerBehaviour)) as TowerBehaviour;
            tb.towerInfo = addedTower;
            GameWorldHandler.Money = GameWorldHandler.Money - addedTower.cost;
            tower = go;

            // Destroy dummy health bars which were created by enemies while no towers existed
            foreach (var bar in GameObject.FindGameObjectsWithTag("DummyHP")) 
            {
                if(bar.name.Contains("(Clone)"))
                    Destroy(bar);
            }

            // Save turret position and hide tile
            GameWorldHandler.turretTiles[tower] = gameObject;
            gameObject.SetActive(false);

            return go;
        }
        return null;
    }
        
    /// <summary>
    /// Exits the shop and removes all listeners from shop buttons
    /// </summary>
    private void ExitShop() 
    {
        unloadTileScripts();
        Shop.SetActive(false);
        GameWorldHandler.isShopOpen = false;
    }

    /// <summary>
    /// Removes all listeners from shop buttons
    /// </summary>
    private void unloadTileScripts() 
    {
        buyTowerButton1.onClick.RemoveAllListeners();
        buyTowerButton2.onClick.RemoveAllListeners();
        buyTowerButton3.onClick.RemoveAllListeners();
        exitShopButton.onClick.RemoveAllListeners();
    }
}