﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TowerBehaviour : MonoBehaviour {

    public TowerClass towerInfo;
    private float waitBetweenShots = 1;
    private float waitTime;
    private GameObject particle;
    public static List<GameObject> allParticles = new List<GameObject>();
    private int waitForThisManyFrames;
    private bool trackClosestEnemy;
    public static Dictionary<GameObject, string> ENEMYHPS = new Dictionary<GameObject, string>();
    private int killCount = 0;
    public static Dictionary<GameObject, GameObject> HPBAR_ENEMY_LINK = new Dictionary<GameObject, GameObject>();
    public GameObject tile;

    // Tower UI
    public GameObject towerInfoBox;
    public Text CurrentType;
    public Text Range;
    public Text FireRate;
    public Text SellTowerButtonText;
    public Button SellTowerButton;

    /// <summary>
    /// Calculates tower fire delay & shooting animation frame lenght. Creates the shooting particle for later use.
    /// </summary>
    void Start() 
    {
        if (towerInfo == null)
            return;
        if (towerInfo.FireSpeed != 0)
            waitBetweenShots = 1 / towerInfo.FireSpeed;
        waitTime = Time.time;
        waitForThisManyFrames = 0;
        trackClosestEnemy = true;

        // Initialize the shooting particle
        particle = Instantiate(GameObject.Find("FireEffect"), transform.position, transform.rotation);
        allParticles.Add(particle);
        particle.SetActive(false);

        // Increment the tower count
        GameWorldHandler.towersOnScreen++;
    }

    /// <summary>
    /// When turret is destroyed, delete the hidden tracer particle as well
    /// </summary>
    void OnDestroy() 
    {
        Destroy(particle);
    }

    /// <summary>
    /// Opens tower info dialog for selling the tower if nothing else is open
    /// </summary>
    public void OnMouseDown()
    {
        if (towerInfo != null && !GameWorldHandler.isShopOpen && !GameWorldHandler.isTurretInfoOpen)
        {
            GameWorldHandler.isTurretInfoOpen = true;
            towerInfoBox.SetActive(true);
            CurrentType.text = "Kills: " + killCount;
            FireRate.text = "Fire rate: " + towerInfo.FireSpeed.ToString();
            Range.text = "Range: " + towerInfo.Range.ToString();
            SellTowerButtonText.text = "Sell: " + towerInfo.cost.ToString();
            SellTowerButton.onClick.AddListener(SellTower);
        }
    }

    /// <summary>
    /// Refunds player the turret cost and removes turret from the game.
    /// </summary>
    public void SellTower() 
    {
        if (towerInfo != null)
        {
            GameWorldHandler.Money = GameWorldHandler.Money + towerInfo.cost;
            towerInfoBox.SetActive(false);
            GameWorldHandler.isTurretInfoOpen = false;
            towerInfo = null;
            if (GameWorldHandler.turretTiles.ContainsKey(gameObject))
            {
                GameWorldHandler.turretTiles[gameObject].SetActive(true);
                GameWorldHandler.turretTiles.Remove(gameObject);  
            }
            Destroy(gameObject);
            GameWorldHandler.towersOnScreen--;

            // If no tower is left to track the enemy health bars, delete them and let enemies carry their own (they show full but whatever)
            if (GameWorldHandler.towersOnScreen == 0)
            {
                foreach (var bar in GameObject.FindGameObjectsWithTag("Enemy"))
                {
                    if (bar.name.Contains("HPFull(Clone)"))
                    {
                        Destroy(bar);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Removes all listeners from shop buttons
    /// </summary>
    private void unloadInfoScripts() 
    {
        SellTowerButton.onClick.RemoveAllListeners();
    }

    /// <summary>
    /// Exits the tower info screen
    /// </summary>
    public void ExitTowerInfo() 
    {
        unloadInfoScripts();
        GameWorldHandler.isTurretInfoOpen = false;
        towerInfoBox.SetActive(false);
    }

    /// <summary>
    /// Check all enemies on the screen, pick the closest one, track them, if they are in range, shoot. Trigger shooting particle, trigger weapon cooldown, display and move enemy health bars.
    /// </summary>
	void LateUpdate() 
	{
        if (towerInfo == null || GameWorldHandler.gameOver) 
            return;

        var enemies = GameWorldHandler.allEnemiesCurrentlyOnScreen;
        var towerPosition = gameObject.transform.position;
        var range = towerInfo.Range;
        GameObject leastDistanceObject = null;

        if (waitForThisManyFrames == 0)
        {
            if (towerInfo.Name != "Rapid-fire" )
            {
                particle.SetActive(false);
            } 
            trackClosestEnemy = true;
        }
        else
        {
            trackClosestEnemy = false;
            waitForThisManyFrames--; 
        }

        float leastDistance = -1;

        foreach (var enemy in enemies)
        {
            var enemyPosition = enemy.transform.position;
            var distance = Vector3.Distance(towerPosition, enemyPosition);

            // Check the list of enemies, if this tower is the first to spot it, add it to the dictionary
            foreach (var type in GameWorldHandler.enemyList)
            {
                if (type.Object.name == enemy.name.Split('(')[0])
                {
                    if (!ENEMYHPS.ContainsKey(enemy))
                    {
                        ENEMYHPS.Add(enemy, type.HP + "/" + type.HP);

                        // Create enemy health bar
                        GameObject HPBar = Instantiate(GameObject.Find("HPFull"), new Vector3(enemy.transform.position.x,enemy.transform.position.y + 0.4f, enemy.transform.position.z) , new Quaternion(0,0,0,0));
                        HPBAR_ENEMY_LINK.Add(enemy, HPBar);
                        break;
                    }
                    break;
                }
            }

            // Move health bars with enemies
            if (HPBAR_ENEMY_LINK.ContainsKey(enemy)) 
            {
                var hpBar = HPBAR_ENEMY_LINK[enemy];
                if (hpBar != null)
                {
                    var maxHP = float.Parse(ENEMYHPS[enemy].Split('/')[1]);
                    var hpNow = float.Parse(ENEMYHPS[enemy].Split('/')[0]);
                    var maxWidth =  GameObject.Find("HPFull").transform.localScale.x;
                    var widthNow = (hpNow / maxHP) * maxWidth;

                    hpBar.transform.localScale = new Vector2(widthNow, hpBar.transform.localScale.y);

                    // HP bar deducts from both ends, so we need to calculate the width change and then move the bar left that much.
                    var difference = maxWidth - widthNow;

                    // Keep HP bar stuck to the left side
                    HPBAR_ENEMY_LINK[enemy] = hpBar;
                    hpBar.transform.position = new Vector3(enemy.transform.position.x -difference/2, enemy.transform.position.y + 0.4f, enemy.transform.position.z);
                }
            }

            if (leastDistance < 0)
            {
                leastDistance = distance;
                leastDistanceObject = enemy;
                continue;
            }
            if (distance < leastDistance)
            {
                leastDistance = distance;
                leastDistanceObject = enemy;
            }
        }

        if (leastDistanceObject != null)
        {
            // Track enemy only if we are in range, otherwise lay dormant
            // Credit: Octet_ at answers.unity3d.com/questions/798707/2d-look-at-mouse-position-z-rotation.c.html
            // ------------------------
            if (leastDistance <= range)
            {
                transform.up = ((Vector2)leastDistanceObject.transform.position - (Vector2)transform.position).normalized;
            }
            else
            {
                if (towerInfo.Name == "Rapid-fire" )
                {
                    particle.SetActive(false);
                } 
            }
            // ------------------------

            // Shooting particle tracks enemy with turret, unless it has just been shot in which case wait x seconds to keep the trail stationary while its showing
            if (trackClosestEnemy) 
            {
                var midPoint = (towerPosition + leastDistanceObject.transform.position)/2;
                particle.transform.localScale = new Vector3(0.05f, leastDistance, 0); 
                particle.transform.position = midPoint;
                particle.transform.rotation = transform.rotation;
            }

            // We shoot if we are in range and we have waited long enough since last shot
            if (leastDistance <= range && waitTime <= Time.time)
            {
                EnemyClass objectType = null;
                foreach (EnemyClass type in GameWorldHandler.enemyList)
                {
                    // Create a firing particle between the turret and the target for a short time, depending on the fire speed of the turret
                    particle.SetActive(true);
                    waitForThisManyFrames = Mathf.RoundToInt(1/towerInfo.FireSpeed*3);

                    // Deduct HP from the enemy using the enemy dictionary
                    if (type.Object.name == leastDistanceObject.name.Split('(')[0]) // Because all copies are named <original>(Clone), I want to fucking die
                    {
                        if (ENEMYHPS.ContainsKey(leastDistanceObject))
                        {
                            ENEMYHPS[leastDistanceObject] = (int.Parse(ENEMYHPS[leastDistanceObject].Split('/')[0]) - 1) + "/" + ENEMYHPS[leastDistanceObject].Split('/')[1];
                            GameWorldHandler.Score = GameWorldHandler.Score + 1;
                        }
                        objectType = type;
                        break;
                    }
                }

                if (ENEMYHPS.ContainsKey(leastDistanceObject))
                {
                    if (int.Parse(ENEMYHPS[leastDistanceObject].Split('/')[0]) <= 0)
                    {
                        // Destroy enemy and its health bar
                        Destroy(HPBAR_ENEMY_LINK[leastDistanceObject]);
                        HPBAR_ENEMY_LINK.Remove(leastDistanceObject);
                        GameWorldHandler.Money = GameWorldHandler.Money + objectType.KillReward;
                        GameWorldHandler.allEnemiesCurrentlyOnScreen.Remove(leastDistanceObject);
                        killCount++;
                        Destroy(leastDistanceObject);
                    }
                }
                waitTime = Time.time + waitBetweenShots;
            }
        }
        else
        {
            if (towerInfo.Name == "Rapid-fire" )
            {
                particle.SetActive(false);
            } 
        }
	}
}